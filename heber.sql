/*
** (C) William Avery 2021
**
** Distributed under the terms of the MIT license.
*/
drop table hb_user;

create table hb_user (
 user_wiki nvarchar(12) not null,
 user_id int(8) unsigned not null,
 user_name varbinary(255) not null,
 user_registration timestamp  not null,
 user_editcount int(11) not null,
 user_lastblock varbinary(14),
 user_lastblock_expiry varbinary(14),
 user_dr_notices tinyint(1),
 user_no_perm_warn tinyint(1),
 user_no_lic_warn tinyint(1),
 user_no_src_warn tinyint(1),
 user_cr_warn tinyint(1),
 user_no_source_warn tinyint(1),
 PRIMARY KEY (user_wiki, user_id)
);
create index ix_user on hb_user(user_name);

drop table hb_page;

create table hb_page (
 page_wiki nvarchar(12) not null,
 page_id int(8) unsigned not null,
 page_namespace int(11),
 page_title varbinary(255),
 page_user_id int(8) unsigned,
 page_draft_timestamp timestamp,
 page_draft_declined timestamp,
 PRIMARY KEY (page_wiki, page_id)
);

create index ix_page on hb_page(page_draft_declined);

drop table hb_image;

create table hb_image (
 image_wiki nvarchar(12) not null,
 image_id int(8) unsigned not null,
 image_namespace int(11),
 image_title varbinary(255),
 image_user_id int(8) unsigned,
 image_lic nvarchar(30),
 image_del_notice tinyint(1),
 image_height int(11),
 image_width int(11),
 image_size int(10) unsigned,
 image_metadata mediumblob,
 image_timestamp timestamp,
 PRIMARY KEY (image_wiki, image_id)
);
 
drop table hb_lic_cat;

create table hb_lic_cat (
 lic_image_wiki nvarchar(12) not null,
 lic_image_id int(8) unsigned not null,
 lic_tag varbinary(255),
 lic_self tinyint(1),
 PRIMARY KEY (lic_image_wiki, lic_image_id, lic_tag)
);

drop table hb_image_link;

create table hb_image_link (
 il_page_wiki nvarchar(12) not null,
 il_page_id int(8) unsigned not null,
 il_image_wiki nvarchar(12),
 il_image_id int(8) unsigned,
 PRIMARY KEY (il_page_wiki, il_page_id, il_image_wiki, il_image_id)
);

