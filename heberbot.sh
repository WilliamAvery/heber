#!/bin/bash
echo Start: `date`
source /data/project/heber/www/python/venv/bin/activate
cd /data/project/heber/www/python/src
python3 heberbot.py --config=heberconfig
echo Finish: `date`
