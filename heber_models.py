#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
# This module is chiefly concerned with data retrieval
# 
from flask import Flask, flash
from flask_caching import Cache
import requests
import pymysql
import json
import mwparserfromhell
from pymysql import err as pymysqlerr
from typing import OrderedDict
from datetime import datetime, timedelta
import humanize
import config

# Simplest way to allow cacheing functions as decorators in this module
# is to create the app here and have app.py import it
app = Flask(__name__)
app.config.from_object(config)
app.config.from_envvar('HEBER_CONFIG') 
cache = Cache(app, config=app.config.get('CACHE_CONFIG', {'CACHE_TYPE': 'NullCache'}))
delete_aliases = app.config.get('DELETE_ALIASES', ['Delete'])
nopermission_aliases = app.config.get('NOPERMISSION_ALIASES', ['No permission since'])
copyvio_aliases = app.config.get('COPYVIO_ALIASES', ['copyvio'])

def get_connection(param_key):
    conn=None
    connparams = app.config.get(param_key)
    if not connparams:
        flash(f"Configuration value for {param_key} is missing!")
    else:
        try:
            conn=pymysql.connect(**connparams)
        except pymysqlerr.OperationalError as e:
            flash(f"{param_key} - {repr(e)}") 
    return conn

def commons_retriever(pagelist, thumb_urls, delete_tags, nopermission_tags, copyvio_tags):

    pageidlist = '|'.join((str(elem['pageid']) for elem in pagelist))
    tmp = requests.get(
        url=app.config['COMMONS_API'],
        params={'action':'query',
            'prop':'imageinfo|revisions',
            'rvprop': 'timestamp|content',
            'iiprop':'url',
            'redirects': 'true',
            'format':'json',
            'iiurlwidth':250,
            'pageids': pageidlist
            }
    )
    response = json.loads(tmp.text)
    for elem  in pagelist:
        pageidstr = str(elem['pageid'])
        if ('query' in response 
        and 'pages' in response['query'] 
        and pageidstr in response['query']['pages']):
            page = response['query']['pages'][pageidstr]
            if 'imageinfo' in page: 
                thumb_urls[elem['pageid']] = page['imageinfo'][0]['thumburl']
            else:
                thumb_urls[elem['pageid']] = ''
            if 'revisions' in page:
                pgtext = page["revisions"][0]["*"]
                mwpfh = mwparserfromhell.parse(pgtext, skip_style_tags=True)
                # delete uncategorised
                for template in mwpfh.filter_templates():
                    if template.name.matches(delete_aliases):
                        delete_tags[elem['pageid']]=True
                    if template.name.matches(nopermission_aliases):
                        nopermission_tags[elem['pageid']]=True
                    if template.name.matches(copyvio_aliases):
                        copyvio_tags[elem['pageid']]=True
    pagelist.clear()

def fetch_decode(cursor):
    columns = cursor.description 
    return [{columns[index][0]:(column.decode() if column and not isinstance(column, str) and columns[index][1] == pymysql.constants.FIELD_TYPE.VAR_STRING else column) for index, column in enumerate(value)} for value in cursor.fetchall()]
 
class FilterQuery():
    def __init__(self, declinestart, declineend, qrange):    
        self.data = None
        self.qrange = qrange
        self.startdate = self.enddate = None
        if not declinestart:
            if qrange:
                flash("Range requires a start date")
            else:
                self.startdate = datetime.today().date()
        else:
            try:
                self.startdate = datetime.strptime(
                    declinestart, "%Y%m%d")
            except ValueError as e:
                flash(repr(e))
        if declineend:
            try:
                self.enddate = datetime.strptime(declineend, "%Y%m%d")
            except ValueError as e:
                flash(repr(e))
        else:
            if qrange:
                flash("Range requires an end date")
        self.echostartdate = self.startdate if self.startdate else None
        self.echoenddate = self.enddate if self.enddate else None
        if qrange and self.startdate > self.enddate:
            flash("Start date must be before end date")
            return None
        if not self.startdate: return None
        if qrange and not self.enddate: return None
        if qrange:
            self.endtime=self.enddate + timedelta(days=1)
        else:
            self.endtime=self.startdate + timedelta(days=1)
        self.data = runfilterquery(self.startdate, self.endtime)

@cache.memoize(timeout=app.config.get('MEMOISE_SECS', 0))
def runfilterquery(starttime, endtime):
    print('runfilterquery')
    hconn = get_connection('HEBER_DB')
    if not hconn:
        return None
    with hconn:
        cursor=hconn.cursor()
        cursor.execute(
            """select u.user_registration as en_user_registration,
                    cu.*,
                    p.*, 
                    i.* 
                from hb_user u 
                join hb_user cu on cu.user_name = u.user_name and cu.user_wiki='commonswiki' and u.user_wiki='enwiki'
                join hb_page p on p.page_user_id = u.user_id 
                join hb_image_link il on p.page_wiki=il.il_page_wiki and p.page_id = il.il_page_id 
                join hb_image i on i.image_id = il.il_image_id and i.image_wiki = i.image_wiki
                where p.page_draft_declined >= %s and p.page_draft_declined < %s""", 
                (starttime, endtime)
        )
        
        result = fetch_decode(cursor)
        pagelist=[]
        thumb_urls={}
        delete_tags={}
        nopermission_tags={}
        copyvio_tags={}
        for page in result:
            pagelist.append({'pageid' : page['image_id'], 'filename': page['image_title']})
            if len(pagelist) == 50 or page == result[-1]:
                commons_retriever(pagelist, thumb_urls, delete_tags, nopermission_tags, copyvio_tags)
        data={}
        for page in result:
            if not page['user_name'] in data:
                data[page['user_name']] = {key: value for key, value in page.items() if key in ('en_user_registration', 'user_registration',  'user_dr_notices', 'user_no_perm_warn','user_no_lic_warn','user_no_src_warn','user_cr_warn','user_no_source_warn')}
                data[page['user_name']]['first_draft_declined'] = page['page_draft_declined']
                data[page['user_name']]['pages']={}
            else:
                if data[page['user_name']]['first_draft_declined'] > page['page_draft_declined']:
                    data[page['user_name']]['first_draft_declined'] = page['page_draft_declined']
            if not page['page_title'] in data[page['user_name']]['pages']:
                data[page['user_name']]['pages'][page['page_title']] = {
                    'page_draft_timestamp': page['page_draft_timestamp'],
                    'page_draft_declined': page['page_draft_declined'],
                    'images': {}
                    }
            images = data[page['user_name']]['pages'][page['page_title']]['images']
            if not page['image_title'] in  images:
                images[page['image_title']] = { 'height': page['image_height'], 
                                                'width': page['image_width'],
                                                'size': humanize.naturalsize(page['image_size'], binary=True), 'thumb': thumb_urls[page['image_id']], 'deleted': thumb_urls[page['image_id']]=='',
                                                'delete_tag': True if page['image_id'] in delete_tags else False,
                                                'nopermission_tag': True if page['image_id'] in nopermission_tags else False,
                                                'copyvio_tag': True if page['image_id'] in copyvio_tags else False,
                                                'lic_tags': []}
                cursor.execute("select lic_tag, lic_self from hb_lic_cat where lic_image_wiki='commonswiki' and lic_image_id = %s", (page['image_id']))
                images[page['image_title']]['lic_tags'] = fetch_decode(cursor)
    return OrderedDict(sorted(data.items(), key=lambda itm: itm[1]['first_draft_declined']))