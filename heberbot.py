#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
import pymysql, pymysql.cursors
import phpserialize
import config, getopt, os, sys

class heberBot(object):
    def __init__(self, config):
        self.runconfig = vars(__import__(config))
        self.dateformat = "'%d-%m-%Y'"
        self.enwikiquery = f"""select p.page_id as page_id, p.page_title as page_title, 
    r1.rev_timestamp as draftdate, 
    a.actor_name as actor_name, 
    u.user_registration as user_registration, 
    timestampdiff(DAY, u.user_registration, r1.rev_timestamp) as userage, 
    il.il_to as image_title,
    cl.cl_timestamp as rejdate,
    timestampdiff(DAY,  cl.cl_timestamp, CURRENT_TIMESTAMP()) as rejage,
    u.user_id as user_id
    from categorylinks cl 
    join page p on p.page_id = cl.cl_from 
    join revision r1 on r1.rev_page = p.page_id 
    join actor a on a.actor_id = r1.rev_actor 
    join user u on u.user_id = a.actor_user
    join imagelinks il on il.il_from_namespace = p.page_namespace and il.il_from = p.page_id 
    where cl.cl_to='Declined_AfC_submissions'
    and p.page_namespace = 118
    and r1.rev_timestamp = (
    select min(rev_timestamp) from revision r where r.rev_page = p.page_id)
    and timestampdiff(DAY, u.user_registration, r1.rev_timestamp) < 100 
    and timestampdiff(DAY,  cl.cl_timestamp, CURRENT_TIMESTAMP()) < 30
    ORDER BY cl.cl_timestamp DESC
    LIMIT 50000
    """
    def existcheck (self, curs, sql, params):
            curs.execute(sql, params)
            return 1 if curs.fetchone() else 0

    
    def run(self): 
        if not 'HEBER_DB' in self.runconfig:
            print("Run config file must contain connection info as 'HEBER_DB'")
        if not 'ENWIKI_DB' in self.runconfig:
            print("Run config file must contain connection info as 'ENWIKI_DB'")
        if not 'COMMONS_DB' in self.runconfig:
            print("Run config file must contain connection info as 'COMMONS_DB'")
        if not ('HEBER_DB' in self.runconfig and 'ENWIKI_DB' in self.runconfig and 'COMMONS_DB' in self.runconfig): 
            exit(2)

        # Load a list of image links in rejected drafts, and close enwiki connection
        enwiki_conn = pymysql.connect(**self.runconfig['ENWIKI_DB'], cursorclass=pymysql.cursors.DictCursor)
        imagelist = None
        with enwiki_conn.cursor() as enwiki_cursor:
            enwiki_cursor.execute(self.enwikiquery)
            imagelist = enwiki_cursor.fetchall()
        enwiki_conn.close()

        commons_conn = pymysql.connect(**self.runconfig['COMMONS_DB'], cursorclass=pymysql.cursors.DictCursor)
        csql = """select p.page_id as image_id,
        a.actor_name as user, 
        u.user_registration as commons_user_registration, 
        r1.rev_timestamp-u.user_registration, 
        u.user_id as image_user_id,
        i.img_size as image_size,
        i.img_height as image_height,
        i.img_width as image_width,
        i.img_metadata as image_metadata,
        if(exists (select 1 from categorylinks cl  where cl.cl_from = p.page_id and cl.cl_to='Self-published_work'), 1, 0) as lic_self
        from page p
        join revision r1 on r1.rev_page = p.page_id 
        join actor a on a.actor_id = r1.rev_actor 
        join user u on u.user_id = a.actor_user
        join image i on i.img_name = p.page_title
        where r1.rev_timestamp = (
        select min(rev_timestamp) from revision r where r.rev_page = p.page_id)
        and p.page_namespace = 6
        and p.page_title=%s
        """
        imgcache = {}
        output = {}

        with commons_conn.cursor() as commons_cursor:
            for img in imagelist:
                image_title = img['image_title']
                print(image_title) # filename

                page_title = img['page_title']

                cdata = None
                if image_title in imgcache:
                    cdata = imgcache[image_title]
                else:
                    commons_cursor.execute(csql, image_title)
                    cdata = commons_cursor.fetchone()
                    imgcache[image_title] = cdata

                if cdata and img['actor_name'] == cdata['user']: # commons uploader was draft creator
                    user = cdata['user']
                    #metadata_obj = phpserialize.loads(cdata['image_metadata'])
                    #print(metadata_obj)
                    print(image_title)
                    if not user in output:
                        output[user] = {'pages': {}, 'user_registration': img['user_registration'],
                                        'commons_user_registration': cdata['commons_user_registration'], 'user_id': img['user_id'], 'commons_user_id': cdata['image_user_id']}
                    if not page_title in output[user]['pages']:
                        output[user]['pages'][page_title] = {'images': {
                        }, 'draftdate': img['draftdate'], 'userage': img['userage'], 'rejdate': img['rejdate'], 'rejage': img['rejage'], 'page_id': img['page_id']}
                    if not image_title in output[user]['pages'][page_title]['images']:
                        output[user]['pages'][page_title]['images'][image_title] = {'image_id': cdata['image_id'], 'image_user_id': cdata['image_user_id'],
                                                                        'image_height': cdata['image_height'], 'image_width': cdata['image_width'], 'image_size': cdata['image_size'], 'image_metadata': cdata['image_metadata'], 'lic_self': cdata['lic_self']}

        print('heber1...')
        heber_conn = pymysql.connect(**self.runconfig['HEBER_DB'])
        print('heber2...')
        loadllsql = ""
        cusql = "SELECT 1 FROM hb_user WHERE user_wiki=%s AND user_id=%s"
        usql = """INSERT INTO hb_user ( user_wiki, user_id, user_name, user_registration, user_dr_notices, user_no_perm_warn, user_no_lic_warn, user_no_src_warn, user_cr_warn, user_editcount) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        ON DUPLICATE KEY UPDATE user_dr_notices=GREATEST(user_dr_notices, %s), user_no_perm_warn= GREATEST(user_no_perm_warn, %s), user_no_lic_warn= GREATEST(user_no_lic_warn, %s), user_no_src_warn=GREATEST(user_no_src_warn, %s), user_cr_warn=GREATEST(user_cr_warn, %s)"""
        cpsql = "SELECT 1 FROM hb_page WHERE page_wiki=%s AND page_id=%s"
        psql = "INSERT INTO hb_page (  page_wiki, page_id , page_namespace , page_title, page_user_id, page_draft_timestamp, page_draft_declined) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cisql = "SELECT 1 FROM hb_image WHERE image_wiki=%s AND image_id=%s"
        isql = """INSERT INTO hb_image (  image_wiki, image_id , image_namespace , image_title, image_user_id, image_height, image_width, image_size, image_metadata) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
        ON DUPLICATE KEY UPDATE image_height=%s, image_width=%s, image_size=%s, image_metadata=%s
        """
        clsql = "SELECT 1 FROM hb_image_link WHERE il_image_wiki=%s AND il_image_id=%s AND il_page_wiki=%s AND il_page_id=%s"
        lsql = "INSERT INTO hb_image_link ( il_image_wiki, il_image_id , il_page_wiki , il_page_id) VALUES (%s, %s, %s, %s)"
        tlwarnsql = "select 1 from page p join templatelinks tl on p.page_id =tl.tl_from and p.page_namespace=tl_from_namespace join linktarget lt on lt.lt_id = tl.tl_target_id and lt_namespace=10 where page_title=%s and page_namespace=3 and lt_title like %s"
        dtsql = "delete from hb_lic_cat where lic_image_wiki=%s and lic_image_id=%s"
        tsql = "insert into hb_lic_cat (lic_image_wiki, lic_image_id, lic_tag, lic_self) values (%s, %s, %s, %s)"
        lictagsql = """select distinct tp.page_title as page_title from page p join templatelinks tl on p.page_id =tl.tl_from and p.page_namespace=tl_from_namespace join linktarget lt on tl.tl_target_id = lt.lt_id and lt_namespace = 10 
        join page tp on tp.page_title=lt.lt_title and tp.page_namespace = lt.lt_namespace join categorylinks cl on tp.page_id =cl.cl_from
        where p.page_title=%s and p.page_namespace=6 and cl_to='Primary_license_tags_(flat_list)'"""
        print('heber3...')
        with heber_conn.cursor() as hcurr, commons_conn.cursor() as ccurr:
            print('hebercursor')
            for user in output.keys():
                user_key=user.replace(b' ', b'_')
                print(('enwiki', output[user]['user_id'], user))
                hcurr.execute(cusql, ('enwiki', output[user]['user_id']))
                if not hcurr.fetchone():
                    hcurr.execute(
                        usql, ('enwiki', output[user]['user_id'], user_key, output[user]['user_registration'], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
                no_perm_warn = self.existcheck(ccurr, tlwarnsql, (user_key, 'Image_permission/%'))
                no_lic_warn = self.existcheck(ccurr, tlwarnsql, (user_key, 'Image_licence/%'))
                no_src_warn = self.existcheck(ccurr, tlwarnsql, (user_key, 'Image_source/%'))
                dr_notices = self.existcheck(ccurr, tlwarnsql, (user_key, 'Idw/%'))
                cr_warn = self.existcheck(ccurr, tlwarnsql, (user_key, 'Copyvionote/%'))
                #hcurr.execute(cusql, ('commonswiki', output[user]['commons_user_id']))
                #if not hcurr.fetchone():
                hcurr.execute(usql, ('commonswiki', output[user]['commons_user_id'], user, output[user]
                            ['commons_user_registration'], dr_notices, no_perm_warn, no_lic_warn, no_src_warn, cr_warn, dr_notices, no_perm_warn, no_lic_warn, no_src_warn, cr_warn, 0))

                for ptitle in output[user]['pages'].keys():
                    print(ptitle)
                    page_title = output[user]['pages'][ptitle]
                    hcurr.execute(cpsql, ('enwiki', page_title['page_id']))
                    if not hcurr.fetchone():
                        hcurr.execute(
                            psql, ('enwiki', page_title['page_id'], 118, ptitle, output[user]['user_id'], page_title['draftdate'], page_title['rejdate']))
                    for ititle in page_title['images'].keys():
                        print(ititle)
                        image_title = page_title['images'][ititle]
                        hcurr.execute(isql, ('commonswiki', image_title['image_id'], 0, ititle, image_title['image_user_id'],
                                        image_title['image_height'], image_title['image_width'], image_title['image_size'], image_title['image_metadata'],
                                        image_title['image_height'], image_title['image_width'], image_title['image_size'], image_title['image_metadata']))
                        hcurr.execute(
                            clsql, ('commonswiki', image_title['image_id'], 'enwiki', page_title['page_id']))
                        if not hcurr.fetchone():
                            hcurr.execute(
                                lsql, ('commonswiki', image_title['image_id'], 'enwiki', page_title['page_id']))
                        hcurr.execute(dtsql, ('commonswiki', image_title['image_id']))
                        ccurr.execute(lictagsql,(ititle))
                        tags = ccurr.fetchall()
                        for tag in tags:
                            print(tag)
                            hcurr.execute(tsql, ('commonswiki', image_title['image_id'], tag['page_title'], image_title['lic_self']))

        commons_conn.close()
        heber_conn.close()
        print(output)

def main():
    config=None
    try:
      opts, args = getopt.getopt(sys.argv[1:],"c:",["config="])
    except getopt.GetoptError:
        print("heberbot --config <configfile>")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ['-c', '--config']:
            config=arg
    if not config:
        print("heberbot --config <configfile>")
        sys.exit(2)
    bot = heberBot(config)
    bot.run()

if __name__ == "__main__":
    main()
