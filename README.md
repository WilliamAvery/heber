# README #

### What is this repository for? ###

* Code for the tool found at [https://heber.toolforge.org](https://heber.toolforge.org)
* This tool is mainly for cleanup of images after article drafts have been rejected on Wikipedia

### How do I get set up? ###

* Original dev has been done on with python 3.8.10 on Ubuntu 20.04.2 LTS
* The live Toolforge Kebernetes environment runs python 3.7.3 Debian 4.19.171-2
* Clone from: git@bitbucket.org:WilliamAvery/heber.git or https://WilliamAvery@bitbucket.org/WilliamAvery/heber.git
* Use Visual Studio Code to open the file heber.code-workspace
* The file `tunnels.sh` or similar needs to be run to set up access to databases on Toolforge

### Who do I talk to? ###

* Leave a message on [my Wikipedia talk page](https://en.wikipedia.org/wiki/User_talk:William_Avery)