#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
# This module is chiefly concerned with routing and controller functionality
# WikiMedia Cloud services prefers and entry point of app.py
# The app object is initialised in heber_models.py and exported back here
# 
from flask import Flask, render_template, flash
from datetime import datetime
import config

from heber_models import FilterQuery, app


@app.route("/drafts/filter/range/To", defaults={'declinestart': '', 'declineend': '', 'qrange': True})
@app.route("/drafts/filter/range/<declinestart>To<declineend>", defaults={'qrange': True})
@app.route("/drafts/filter/range/<declinestart>To", defaults={'qrange': True, 'declineend': ''})
@app.route("/drafts/filter/range/To<declineend>", defaults={'qrange': True, 'declinestart': ''})
@app.route("/drafts/filter/range", defaults={'declinestart': '', 'declineend': '', 'qrange': True})


def qrange(declinestart, declineend, qrange):
    return filter(declinestart, declineend, qrange)


@app.route("/", defaults={'declinestart': '', 'declineend': '', 'qrange': False})
@app.route("/drafts/filter", defaults={'declinestart': '', 'declineend': '', 'qrange': False})
@app.route("/drafts/filter/<declinestart>", defaults={'declineend': '', 'qrange': False})
def filter(declinestart, declineend, qrange):
    return render_template('drafttable.html.jinja', result=FilterQuery(declinestart, declineend, qrange), bp='md')

# These are used by jinja. Parked here because it's not worth having a separate module
@app.template_filter('fmtdate')
def fmtdate(value, format='%d/%m/%Y'):
    return(value.strftime(format)) if value else ''

@app.template_filter('linktext')
def linktext(value):
    return(value.replace('_', ' ')) if value else ''

@app.context_processor
def context_config():
    # This will intentionally fail if CONTEXT_CONFIG key not in config
    return {'config': {key: app.config[key] for key in app.config['CONTEXT_CONFIG']}}
