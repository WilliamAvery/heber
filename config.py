#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
import os

CACHE_CONFIG = {'CACHE_TYPE': 'simple'}

SECRET_KEY = os.urandom(12).hex()
USER_AGENT = "Heber 0.1"
API_PATH = "https://en.wikipedia.org/w/api.php"
COMMONS_API="https://commons.wikimedia.org/w/api.php"

MEMOISE_SECS=600
# Appear in template context as config.COMMONSURL, etc
CONTEXT_CONFIG = ['COMMONSURL', 'ENURL', 'COMMONSFULLPATH', 'ENLOGO', 'COMMONSLOGO']

DELETE_ALIASES = ["Delete", "Del", "Deletebecause", "Puf", "Ffd", "Grise", "Fshije", "Nfd", "অপসারণ"]          
NOPERMISSION_ALIASES = ["No permission since", "No permission", "Permission missing", "Di-no permission"]          

COMMONSURL ='https://commons.wikimedia.org/wiki/'
ENURL='https://en.wikipedia.org/wiki/'
COMMONSFULLPATH='https://commons.wikimedia.org/w/index.php?title='
ENLOGO = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Wikipedia-logo-v2.svg/32px-Wikipedia-logo-v2.svg.png"
COMMONSLOGO="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/100px-Commons-logo.svg.png"