# Sample script to set up tunnels to mysql instances
#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
ssh  -L :4711:enwiki.analytics.db.svc.wikimedia.cloud:3306 -N williamavery@login.toolforge.org -i  ~/Documents/willaverydotnet.txt &

ssh  -L :4713:commonswiki.analytics.db.svc.wikimedia.cloud:3306 -N williamavery@login.toolforge.org -i  ~/Documents/willaverydotnet.txt &

ssh  -L :4712:tools.db.svc.wikimedia.cloud:3306 -N williamavery@login.toolforge.org -i  ~/Documents/willaverydotnet.txt &

