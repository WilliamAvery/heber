#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
CACHE_CONFIG = {'CACHE_TYPE': 'simple'}
HEBER_DB = {'host':"localhost",
            'port':4712,
            'read_default_file':"~/heber.extra",
            'database':"s54800__heber_p",
            'autocommit':True}
# Below are only used by bot
ENWIKI_DB = {'host':"localhost",
             'port':4711,
             'read_default_file':"~/heber.extra",
             'database':"enwiki_p"}
COMMONS_DB = {'host':"localhost",
             'port':4713,
             'read_default_file':"~/heber.extra",
             'database':"commonswiki_p"}