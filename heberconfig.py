#
# (C) William Avery 2021
#
# Distributed under the terms of the MIT license.
#
CACHE_CONFIG = {'CACHE_TYPE': 'simple'}
HEBER_DB = {'host':"tools.db.svc.wikimedia.cloud",
            'read_default_file':"~/replica.my.cnf",
            'database':"s54800__heber_p",
            'autocommit':True}

# Below are only used by bot

ENWIKI_DB = {'host':"enwiki.analytics.db.svc.wikimedia.cloud",
            'read_default_file':"~/replica.my.cnf",
            'database':"enwiki_p",
            'autocommit':True}

COMMONS_DB = {'host':"commonswiki.analytics.db.svc.wikimedia.cloud",
            'read_default_file':"~/replica.my.cnf",
            'database':"commonswiki_p",
            'autocommit':True}
